# Rennrad Packliste

## Am Rad

- Tacho
- Oberrohrtasche
- Arschrakete ?
- 2 Flaschen => 1,9l

## Werkzeug und Wartung ?

- Werkzeugtasche
- Multitool
- Speichenspanner
- Flickzeug
- Lappen
- Öl
- 2 Reifenheber
- Schlauch
- Kryptonite

## Hygiene

- Zeckenkarte
- Schmerztabletten
- Sonnenschutz
- Aloe Vera
- Zahnbürste
- Zahnpasta
- Duschgel
- Creme
- Haarspray
- Desinfektionsspray
- Handseife
- Waschmittel
- Taschenmesser
- Kühltuch

## Radbekleidung

- Trikot kurz 3x
- Trägerhose 2x
- Fahrradunterhose 1x ?
- Socken 3x
- Halstuch 1x
- Kopfhaube
- Fahrradhelm
- Fahrradschuhe
- Handschuhe
- Jacke
- Regenjacke

## Zubehör

- Headset
- USB-C Kabel
- Micro-USB Kabel
- Netzteil
- Brustgurt
- Sonnenbrille
- Taschentücher

## Bekleidung

- Rote Turnschuhe
- Cap
- 4 paar Socken
- 5 Unterhosen
- 4 T-Shirts
- 2 gute kurze Hosen
- 1 lange Hosen
- 1 Sporthose
- 1 Puma Jacke
- Gürtel
